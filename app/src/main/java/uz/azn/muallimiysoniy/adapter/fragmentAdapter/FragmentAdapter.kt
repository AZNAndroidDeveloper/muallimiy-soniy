package uz.azn.muallimiysoniy.adapter.fragmentAdapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class FragmentAdapter(fragmentManager: FragmentManager):FragmentPagerAdapter(fragmentManager) {
   val fragmentList:MutableList<Fragment> = arrayListOf()
    val title:MutableList<String> = arrayListOf()

    override fun getItem(position: Int): Fragment  = fragmentList[position]

    override fun getCount(): Int  = fragmentList.size

    override fun getPageTitle(position: Int): CharSequence? {
         return title[position]
    }

    fun addFragment(fragment:Fragment,name:String){
        fragmentList.add(fragment)
        title.add(name)
    }
}