package uz.azn.muallimiysoniy.adapter.recycleViewAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import uz.azn.muallimiysoniy.R
import uz.azn.muallimiysoniy.model.Info

class RecycleViewAdapter(val listInfo: MutableList<Info>) :
    RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder>() {
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val lesson = itemView.findViewById<TextView>(R.id.lesson)
        val number = itemView.findViewById<TextView>(R.id.text_number)
        fun bind(info: Info) {
            number.text = info.id.toString()
            lesson.text = info.name.toString()

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.items_layout, null, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = listInfo.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(listInfo[position])
    }
}