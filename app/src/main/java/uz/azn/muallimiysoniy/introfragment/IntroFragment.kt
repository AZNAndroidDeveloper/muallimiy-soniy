package uz.azn.muallimiysoniy.introfragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import uz.azn.muallimiysoniy.R
import uz.azn.muallimiysoniy.adapter.fragmentAdapter.FragmentAdapter
import uz.azn.muallimiysoniy.adapter.recycleViewAdapter.RecycleViewAdapter
import uz.azn.muallimiysoniy.firstFragment.FirstFragment
import uz.azn.muallimiysoniy.model.Info
import uz.azn.muallimiysoniy.secondFragment.SecondFragmentFragment

class IntroFragment(var mContext :Context?) : Fragment() {

    init {
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val view =  inflater.inflate(R.layout.fragment_intro, container, false)
        val fragmentAdapter = FragmentAdapter(fragmentManager!!)
        fragmentAdapter.addFragment(FirstFragment(),"Nazariy")
        fragmentAdapter.addFragment(SecondFragmentFragment(mContext),"Test")
        val viewPager = view.findViewById<ViewPager>(R.id.viewPager)
        val tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
        viewPager.adapter = fragmentAdapter
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.getTabAt(0)!!.setIcon(R.drawable.ic_menu_book_)
        tabLayout.getTabAt(1)!!.setIcon(R.drawable.ic_grading)
        return  view
    }



}