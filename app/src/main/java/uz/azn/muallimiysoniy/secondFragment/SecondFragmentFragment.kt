package uz.azn.muallimiysoniy.secondFragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import uz.azn.muallimiysoniy.R
import uz.azn.muallimiysoniy.adapter.recycleViewAdapter.RecycleViewAdapter
import uz.azn.muallimiysoniy.model.Info


class SecondFragmentFragment(var mContext :Context?) : Fragment() {

    init {
        mContext = context
    }
    lateinit var infoList:MutableList<Info>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val view = inflater.inflate(R.layout.fragment_second_fragment, container, false)
        val recycleViewAdapter = RecycleViewAdapter(getItem())
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycle)
        recyclerView.layoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        recyclerView.adapter = recycleViewAdapter

        return  view
    }
    fun getItem():MutableList<Info>{
        infoList = arrayListOf()
        infoList.add(Info(1,"A xarfi"))
        infoList.add(Info(2,"B xarfi"))
        infoList.add(Info(3,"C xarfi"))
        infoList.add(Info(4,"D xarfi"))
        infoList.add(Info(5,"E xarfi"))
        infoList.add(Info(6,"F xarfi"))
        infoList.add(Info(7,"G xarfi"))
        infoList.add(Info(8,"H xarfi"))
        infoList.add(Info(9,"I xarfi"))
        infoList.add(Info(10,"J xarfi"))
        infoList.add(Info(11,"K xarfi"))
        infoList.add(Info(12,"L xarfi"))
        infoList.add(Info(13,"M xarfi"))
        infoList.add(Info(14,"O xarfi"))
        infoList.add(Info(15,"P xarfi"))
        infoList.add(Info(16,"Q xarfi"))
        infoList.add(Info(17,"R xarfi"))
        infoList.add(Info(18,"S xarfi"))
        infoList.add(Info(19,"T xarfi"))
        infoList.add(Info(20,"V xarfi"))
        return infoList
    }

}